.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Flat-File Database, run this command in your terminal:

.. code-block:: console

    $ pip install ffdb

This is the preferred method to install Flat-File Database, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Flat-File Database can be downloaded from the `Bitbucket repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git@bitbucket.org:hkmshb/ffdb.git

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://bitbucket.org/hkmshb/ffdb/get/master.tar.gz

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Bitbucket repo: https://bitbucket.org/hkmshb/ffdb
.. _tarball: https://bitbucket.org/hkmshb/ffdb/get/master.tar.gz
