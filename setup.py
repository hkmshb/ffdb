import os, codecs
from setuptools import setup, find_packages


HERE = os.path.abspath(os.path.dirname(__file__))

def read(fname):
    with codecs.open(os.path.join(HERE, fname), 'r') as fp:
        return fp.read()


requirements = [ ]
test_requirements = ['pytest', 'pytest-cov']

# expose metadata dunder vars in ffdb/__meta__.py
exec(read('ffdb/__meta__.py'))

setup(
    name='ffdb',
    version=__version__,
    url='https://bitbucket.org/hkmshb/ffdb',

    author=__author__,
    author_email=__email__,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    description=__description__,
    extras_require={
        'test': test_requirements
    },
    install_requires=requirements,
    license="BSD license",
    long_description=read('README.md') + '\n\n' + read('CHANGES.md'),
    include_package_data=True,
    keywords='database nosql flat-file',
    packages=find_packages(include=['ffdb']),
    tests_require=test_requirements,
    test_suite='tests',
    zip_safe=False,
)
